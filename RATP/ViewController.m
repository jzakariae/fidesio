//
//  ViewController.m
//  RATP
//
//  Created by zakaria on 07/06/2017.
//  Copyright © 2017 FIDESIO. All rights reserved.
//

#import "ViewController.h"
#import "Record.h"

#define BASEURL "https://data.ratp.fr"
#define URI "/api/records/1.0/search"
#define DATASET "liste-des-commerces-de-proximite-agrees-ratp"

@interface ViewController (){
    
    
    
    
}

@property (nonatomic, strong) NSArray *records;

@end

@implementation ViewController

@synthesize mapView,viewPage,tableView,titre,heightConstraint;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.mapView.showsUserLocation = YES;
    [self.mapView setCenterCoordinate:mapView.userLocation.location.coordinate animated:YES];
    

    
    [self configureRestKit];
    [self loadRecords:48.8520930694:2.34738897685];
}



- (IBAction)hideShow:(id)sender{
    
    if(self.heightConstraint.constant == -40)
        self.heightConstraint.constant = 20-[[UIScreen mainScreen] bounds].size.height;
    else
        self.heightConstraint.constant = -40;
}


#pragma mark - Location

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation {
    
    CLLocationCoordinate2D loc = [aUserLocation coordinate];
    [self loadRecords:loc.latitude:loc.longitude];
    
}


-(void) updateMap :(float) lat :(float)lon {
    
    // add Map zoom
    CLLocationCoordinate2D location;
    location.latitude = lat;
    location.longitude = lon;
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.01;
    span.longitudeDelta = 0.01;
    region.span = span;
    region.center = location;
    [self.mapView setRegion:region animated:YES];
    
    
    for(int i=0 ; i<_records.count ; i++ ){
        
        // Add an annotation
        Record *record = _records[i];
        
        CLLocationCoordinate2D pinCoordinate;
        pinCoordinate.latitude = [record.fields.coord_geo[0] doubleValue];
        pinCoordinate.longitude = [record.fields.coord_geo[1] doubleValue];

        
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        point.coordinate  = pinCoordinate;
        point.title = [NSString stringWithFormat:@"%@", record.fields.tco_libelle];
    
        [self.mapView addAnnotation:point];
        
    }
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _records.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] ;
    
    if (cell == nil)  {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    Record *record = _records[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@", record.fields.tco_libelle];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ cp:%@",
                                 record.fields.ville,record.fields.code_postal];
    
    return cell;
}


#pragma mark - configure RestKit

- (void)configureRestKit
{
    NSString *url = [NSString stringWithUTF8String:BASEURL];
    NSString *uri = [NSString stringWithUTF8String:URI];
    
    // initialize AFNetworking HTTPClient
    NSURL *baseURL = [NSURL URLWithString:url];
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    
    // initialize RestKit
    RKObjectManager *objectManager = [[RKObjectManager alloc] initWithHTTPClient:client];
    
    // setup object mappings
    RKObjectMapping *recordMapping = [RKObjectMapping mappingForClass:[Record class]];
    [recordMapping addAttributeMappingsFromArray:@[@"recordid"]];
    
    // define location object mapping
    RKObjectMapping *fieldMapping = [RKObjectMapping mappingForClass:[Field class]];
    [fieldMapping addAttributeMappingsFromArray:@[@"ville", @"code_postal", @"tco_libelle", @"coord_geo"]];
    
    // define relationship mapping
    [recordMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"fields" toKeyPath:@"fields" withMapping:fieldMapping]];
    
    
    // register mappings with the provider using a response descriptor
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:recordMapping method:RKRequestMethodGET pathPattern:uri keyPath:@"records" statusCodes:[NSIndexSet indexSetWithIndex:200]];
    
    [objectManager addResponseDescriptor:responseDescriptor];
    
}

- (void)loadRecords:(float) lat :(float)lon
{
    
    NSString *dataset = [NSString stringWithUTF8String:DATASET];
    NSString *uri = [NSString stringWithUTF8String:URI];
    
    
    NSString *latLon =[NSString stringWithFormat:@"%f,%f,500", lat,lon];
    
    NSDictionary *queryParams;
    queryParams = [NSDictionary dictionaryWithObjectsAndKeys:dataset, @"dataset", latLon, @"geofilter.distance", nil];
    
    [[RKObjectManager sharedManager] getObjectsAtPath:uri parameters:queryParams success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        
        _records = mappingResult.array;
        [self.tableView reloadData];
        [self updateMap:lat:lon];
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        NSLog(@"error': %@", error);
    }];
}


@end
