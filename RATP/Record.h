//
//  Record.h
//  RATP
//
//  Created by zakaria on 07/06/2017.
//  Copyright © 2017 FIDESIO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Field.h"

@interface Record : NSObject
@property (nonatomic, strong) NSString *recordid;
@property (nonatomic, strong) Field *fields;
@end
