//
//  Field.h
//  RATP
//
//  Created by zakaria on 07/06/2017.
//  Copyright © 2017 FIDESIO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Field : NSObject

@property (nonatomic, strong) NSString *ville;
@property (nonatomic, strong) NSString *code_postal;
@property (nonatomic, strong) NSString *tco_libelle;
@property (nonatomic, strong) NSArray * coord_geo;
@end
