//
//  AppDelegate.h
//  RATP
//
//  Created by zakaria on 07/06/2017.
//  Copyright © 2017 FIDESIO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

