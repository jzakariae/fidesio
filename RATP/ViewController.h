//
//  ViewController.h
//  RATP
//
//  Created by zakaria on 07/06/2017.
//  Copyright © 2017 FIDESIO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>
#import <MapKit/MapKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIView *viewPage;
@property (weak, nonatomic) IBOutlet UIButton *titre;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraint;

- (IBAction)hideShow:(id)sender;

@end

